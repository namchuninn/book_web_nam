﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookWeb.Startup))]
namespace BookWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
